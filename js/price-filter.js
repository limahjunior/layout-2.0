// Price Range
var price = document.getElementById('price');
noUiSlider.create(price, {
    start: [100, 500],
    connect: true,
    range: {
        'min': 0,
        'max': 1000
    }
});
price.noUiSlider.on('update', function (values, handle) {
    var value = values[handle];
    if (handle) {
        $('#max-price').text('R$' + Math.round(value));
        $('input[name="max-price"]').text(Math.round(value));
    } else {
        $('#min-price').text('R$' + Math.round(value));
        $('input[name="min-price"]').text(Math.round(value));
    }
});