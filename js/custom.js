// Home banner slider ========================================================================
$('#myCarousel').carousel()
// Modais ========================================================================
$('#certificadoModal').modal('hide')
$('#condpgtoModal').modal('hide')
$('#calculafreteModal').modal('hide')
// Function to check element is exist ========================================================
$.fn.exist = function () {
    return $(this).length > 0;
}
// Function to set color & style =============================================================
function set_color() {
    var color = localStorage.getItem('dd-color');
    var style = localStorage.getItem('dd-style');
    $('#color-chooser').val(color);
    $('#style-chooser').val(style);
    $('#theme').attr('href', 'css/style.' + color + '.' + style + '.css');
    $('.logo img').attr('src', 'images/logo-' + color + '.png');
}
// Function to get current scroll position ===================================================
function get_current_scroll() {
    return document.scrollTop || document.documentElement.scrollTop || document.body.scrollTop;
}
// Wrap IIFE around the code
(function ($, viewport) {
    $(function () {
        // Change Color Style ======================================================================
        $('.chooser-toggle').click(function () {
            $('.chooser').toggleClass('chooser-hide');
        });
        if (localStorage.getItem('dd-color') == null) {
            localStorage.setItem('dd-color', 'teal');
        }
        if (localStorage.getItem('dd-style') == null) {
            localStorage.setItem('dd-style', 'flat');
        }
        set_color();
        $('#color-chooser').change(function () {
            localStorage.setItem('dd-color', $(this).val());
            set_color();
        });
        $('#style-chooser').change(function () {
            localStorage.setItem('dd-style', $(this).val());
            set_color();
        });
        // Sticky Middle Header ====================================================================
        var lastScrollTop = 0;
        $(window).scroll(function () {
            var cs = get_current_scroll();
            if (cs > lastScrollTop) {
                // scroll down
                var limiter = 170
            } else {
                // scroll up
                var limiter = 64
            }
            lastScrollTop = cs;
            var top_header = $('.top-header');
            var middle_header = $('.middle-header');
            var middle_header_row = $('.middle-header .row');
            var logo_img = $('.logo img');
            var logo_h4 = $('.logo h4');
            var logo_a = $('.logo a');
            var search_box = $('.search-box');
            var cart_btn = $('.cart-btn');
            logo_h4.remove();
            if (cs > limiter) {
                if (viewport.is('<=sm')) {
                    top_header.addClass('mt-139');
                    middle_header_row.addClass('pb-10');
//                    logo_img.addClass('hide');
//                    logo_a.append('<h4>' + logo_img.data(' text-logo') + '</h4>');
                    search_box.add(cart_btn).removeClass('m-t-2');
                } else {
                    top_header.addClass('mt-94');
                }
                middle_header.addClass('sticky');
            } else {
                top_header.removeClass('mt-94');
                top_header.removeClass('mt-139');
                middle_header_row.removeClass('pb-10');
                logo_img.removeClass('hide');
                middle_header.removeClass('sticky');
                search_box.add(cart_btn).addClass('');
            }
        });
        // open dropdown on hover for desktop only =================================================
        $('ul.nav li.dropdown').hover(function () {
            if (viewport.is('>xs')) {
                $(this).addClass('open');
            }
        }, function () {
            if (viewport.is('>xs')) {
                $(this).removeClass('open');
            }
        });
        // Navigation submenu ======================================================================
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
        // owlCarousel for Home Slider =============================================================
        if ($('.home-slider').exist()) {
            $('.home-slider').owlCarousel({
                items: 1,
                loop: true,
                autoplay: true,
                autoplayHoverPause: true,
                dots: false,
                nav: true,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            });
        }
        // owlCarousel for Widget Slider ===========================================================
        if ($('.widget-slider').exist()) {
            var widget_slider = $('.widget-slider');
            widget_slider.owlCarousel({
                items: 1,
                loop: true,
                autoplay: true,
                autoplayHoverPause: true,
                dots: false,
                nav: true,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                responsive: {
                    0: {
                        items: 2,
                    },
                    768: {
                        items: 3,
                    },
                    992: {
                        items: 1,
                    }
                }
            });
        }
        // owlCarousel for Banners Slider ===========================================================
        if ($('.banners-slider').exist()) {
            var widget_slider = $('.banners-slider');
            widget_slider.owlCarousel({
                items: 1,
                loop: true,
                autoplay: true,
                autoplayHoverPause: true,
                dots: true,
                nav: false,
                responsive: {
                    0: {
                        items: 2,
                    },
                    768: {
                        items: 3,
                    },
                    992: {
                        items: 1,
                    }
                }
            });
        }
        // owlCarousel for Product Slider ==========================================================
        if ($('.product-slider').exist()) {
            var product_slider = $('.product-slider')
            product_slider.owlCarousel({
                dots: false,
                nav: true,
                loop: true,
                autoplay: true,
                autoplayHoverPause: true,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                responsive: {
                    0: {
                        items: 2,
                    },
                    768: {
                        items: 6,
                    },
                    1200: {
                        items: 6,
                    }
                }
            });
        }
        // owlCarousel for Product Slider 2 =========================================================
        if ($('.product-slider2').exist()) {
            var product_slider = $('.product-slider2')
            product_slider.owlCarousel({
                dots: false,
                nav: true,
                loop: true,
                autoplay: true,
                autoplayHoverPause: true,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                responsive: {
                    0: {
                        items: 2,
                    },
                    768: {
                        items: 6,
                    },
                    1200: {
                        items: 6,
                    }
                }
            });
        }
        // owlCarousel for Related Product Slider =================================================
        if ($('.related-product-slider').exist()) {
            var related_product_slider = $('.related-product-slider')
            related_product_slider.owlCarousel({
                dots: false,
                nav: true,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                responsive: {
                    0: {
                        items: 2,
                    },
                    768: {
                        items: 3,
                    },
                    992: {
                        items: 5,
                    },
                    1200: {
                        items: 6,
                    }
                }
            });
        }
        // owlCarousel for Brand Slider ============================================================
        if ($('.brand-slider').exist()) {
            var brand_slider = $('.brand-slider');
            brand_slider.owlCarousel({
                dots: false,
                loop: true,
                autoplay: true,
                autoplayHoverPause: true,
                nav: true,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                responsive: {
                    0: {
                        items: 2,
                        margin: 10
                    },
                    480: {
                        items: 2,
                        margin: 15
                    },
                    768: {
                        items: 3,
                        margin: 15
                    },
                    992: {
                        items: 4,
                        margin: 30
                    },
                    1200: {
                        items: 6,
                        margin: 30
                    }
                }
            });
        }
        // Tooltip =================================================================================
        $('button[data-toggle="tooltip"]').tooltip({container: 'body', animation: false});
        $('a[data-toggle="tooltip"]').tooltip({container: 'body', animation: false});
        // Back top Top ============================================================================
        $(window).scroll(function () {
            if ($(this).scrollTop() > 70) {
                $('.back-top').fadeIn();
            } else {
                $('.back-top').fadeOut();
            }
        });
        // Touchspin ===============================================================================
        if ($('.input-qty').exist()) {
            $('.input-qty input').TouchSpin({
                verticalbuttons: true,
                prefix: 'qtd'
            });
        }
        // Typeahead example =======================================================================
        $('.search-input').typeahead({
            fitToElement: true,
            source: [
                '123',
                '321',
            ]
        });
        // Function to set owl cover height via data-*breakpoint*-height ===========================
        function set_owl_cover_height() {
            $('.owl-cover').each(function () {
                $(this).css('background-image', 'url(' + decodeURIComponent($(this).data('src')) + ')');
                if (viewport.is('>=xs')) {
                    if ($(this).attr('data-xs-height')) {
                        $(this).css('height', $(this).data('xs-height'));
                    }
                }
                if (viewport.is('>=sm')) {
                    if ($(this).attr('data-sm-height')) {
                        $(this).css('height', $(this).data('sm-height'));
                    }
                }
                if (viewport.is('>=md')) {
                    if ($(this).attr('data-md-height')) {
                        $(this).css('height', $(this).data('md-height'));
                    }
                }
                if (viewport.is('>=lg')) {
                    if ($(this).attr('data-lg-height')) {
                        $(this).css('height', $(this).data('lg-height'));
                    }
                }
            });
        }
        // set owl cover height ====================================================================
        if ($('.owl-cover').exist()) {
            set_owl_cover_height();
            $(window).resize(function () {
                set_owl_cover_height();
            });
        }
    });
})(jQuery, ResponsiveBootstrapToolkit);
/*----------------------------------------------------*/
//* Google javascript api v3  -- map */
/*----------------------------------------------------*/
(function () {
    "use strict";
    function initialize() {
        /* change your with your coordinates */
        var myLatLng = new google.maps.LatLng(-23.4824378, -47.4733898), // Your coordinates
                mappy = {
                    center: myLatLng,
                    zoom: 18,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: [{
                            "elementType": "geometry",
                            "stylers": [{
                                    "hue": "#000"
                                }, {
                                    "weight": 1
                                }, {
                                    "saturation": -200
                                }, {
                                    "gamma": 0.70
                                }, {
                                    "visibility": "on"
                                }]
                        }]
                };
        var map = new google.maps.Map(document.getElementById("map"), mappy),
                newpin = './images/pin.png';
        new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: newpin,
            animation: google.maps.Animation.DROP,
            title: 'DDMáquinas' // Title for marker
        });
    }
    if (document.getElementById("map")) {
        google.maps.event.addDomListener(window, 'load', initialize);
    }
}());